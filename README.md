## Requirements
```
python
postgresql
rabbitmq
```

## Initial Setup
```
python -m venv lenv
source lenv/bin/activate
pip install --upgrade pip
pip install -r requirements.txt
cp config/database.yml.example config/database.yml # correct it manually 
python manage.py migrate
python manage.py seed
```

## Run Developer mode
```
celery -A edukaka worker -l info
python manage.py runserver
```

## Deploy
```
fab deploy
fab stop
fab start
```
