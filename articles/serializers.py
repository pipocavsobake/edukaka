from rest_framework import serializers
from articles.models import Article

class ArticleSerializer(serializers.ModelSerializer):
    class Meta:
        model = Article
        # fields = ('id', 'name', 'source', 'json_file', 'created_at', 'updated_at')
        fields = '__all__'
