from django.db import models

# Create your models here.
class Article(models.Model):
    name = models.CharField(max_length=255)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    source = models.FileField(upload_to='article/source/%Y/%m/%d/', blank=True)
    json_file = models.FileField(upload_to='article/json_file/%Y/%m/%d/', blank=True)

