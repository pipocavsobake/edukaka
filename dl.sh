ssh edu@shirykalov.ru "/usr/lib/postgresql/10/bin/pg_dump -Fc edu -p 5433 --no-owner >/home/edu/tmp.bak"
rsync -av --progress edu@shirykalov.ru:/home/edu/tmp.bak tmp/tmp.bak
local ops="-dedukaka_development"
if [[ $1 != "" ]]; then
  ops=$1
fi
pg_restore --no-owner -Fc -c $ops tmp/tmp.bak

