from django.contrib import admin
from .models import Course, Statement, Concept
from .tasks import parse_tex_file
# Register your models here.


def refresh_json(modeadmin, request, queryset):
    # import code; code.interact(local=dict(globals(), **locals()))
    for course in queryset.all():
        parse_tex_file.delay(course.id)
class CourseAdmin(admin.ModelAdmin):
    actions = [refresh_json]

admin.site.register(Course, CourseAdmin)
admin.site.register(Statement)
admin.site.register(Concept)

