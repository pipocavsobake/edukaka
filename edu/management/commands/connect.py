from django.core.management.base import BaseCommand, CommandError
from tqdm import tqdm
from edu.models import Statement

class Command(BaseCommand):
    help = "Connect statements by refs"

    def handle(self, *args, **options):
        self.stdout.write('connecting data...')
        run_connect(self)
        self.stdout.write('done.')


def run_connect(cmd):
    """ Connect Statement based on refs

    :return:
    """
    # for statement in tqdm(Statement.objects.all()):
        # statement.manual_tex
