from django.core.management.base import BaseCommand, CommandError
from edu.models import Course, Statement, Concept
import logging
logger = logging.getLogger(__name__)
from prompt_toolkit.shortcuts import yes_no_dialog
from glob import glob
from os.path import basename
from tqdm import tqdm
from django.contrib.auth.models import UserManager, User
import sys

from TexSoup import TexSoup
class Command(BaseCommand):
    help = "seed database for testing and development."

    def add_arguments(self, parser):
        parser.add_argument('--mode', type=str, help="Mode")

    def handle(self, *args, **options):
        self.stdout.write('seeding data...')
        run_seed(self, options['mode'])
        self.stdout.write('done.')


def clear_data():
    """Deletes all the table data"""
    if not yes_no_dialog(title="", text="Do you really want to destroy all data?"):
        return
    logger.info("Delete Address instances")
    Course.objects.all().delete()
    Statement.objects.all().delete()
    Concept.objects.all().delete()
    User.objects.all().delete()

def get_labels(data):
    try:
        soup = TexSoup(data)
        return [l for l in soup.children if l.name == 'label']
    except:
        print(data)
        return []

def get_refs(data):
    try:
        soup = TexSoup(data)
        return [l for l in soup.find_all('ref')]
    except:
        print(data)
        return []


def create_data():
    """Creates an address object combining different elements from the list"""
    logger.info("Creating address")
    course, ok = Course.objects.get_or_create(name="Матан")
    path_tmpl = '/home/anatoly/workspaces/Tex/Tex/Calculus-Galat/theorem/*'
    for d_path in tqdm(glob(path_tmpl)):
        kind = basename(d_path)
        for fp in tqdm(glob("{}/*".format(d_path))):
            with open(fp, "r") as f:
                data = f.read()
                labels = get_labels(data)
                if len(labels) == 0:
                    course.statement_set.get_or_create(manual_tex=data, kind=kind)
                else:
                    course.statement_set.get_or_create(manual_tex=data, kind=kind, name=labels[0].args[0])
    print("connect statements\n")
    for statement in tqdm(Statement.objects.all()):
        refs = statement.try_refs()
        if len(refs) != 0:
            statement.used_statements.set(Statement.objects.filter(name__in=refs))


def create_users():
    User.objects.create_superuser("admin", "", "12345678")
    User.objects.create_user("common", "", "12345678", is_staff=True)


def run_seed(self, mode):
    """ Seed database based on mode

    :param mode: refresh / clear
    :return:
    """
    if mode == 'clear':
        clear_data()
        return

    create_users()
    create_data()
