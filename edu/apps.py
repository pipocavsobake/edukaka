from django.apps import AppConfig


class EduConfig(AppConfig):
    name = 'edu'
    def ready(self):
        import edu.signals
