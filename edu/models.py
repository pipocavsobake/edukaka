from django.db import models
from TexSoup import TexSoup

class BaseModel(models.Model):
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    class Meta:
        abstract = True

class Course(BaseModel):
    name = models.CharField(max_length=255)
    source = models.FileField(upload_to='course/source/%Y/%m/%d/', blank=True)
    json_file = models.FileField(upload_to='course/json_file/%Y/%m/%d/', blank=True)
    def __str__(self):
        return self.name
    class Meta:
        db_table = u'courses'
    def tex_soup(self):
        with open(self.source.path) as src:
            return TexSoup(src.read())

class Statement(BaseModel):
    course = models.ForeignKey('Course', on_delete=models.CASCADE)
    kind = models.CharField(max_length=255)
    name = models.CharField(max_length=255, db_index=True, unique=True, null=True)
    manual_tex = models.TextField()
    comment = models.TextField(max_length=255)
    # В определении системы линейных уравнений определяется много определяемых понятий
    concepts = models.ManyToManyField('Concept', blank=True)
    used_concepts = models.ManyToManyField('Concept', through='ConceptUsage', related_name='used_concepts', blank=True)

    parent = models.ForeignKey('Statement', on_delete=models.CASCADE, blank=True, null=True, related_name="statements")

    used_statements = models.ManyToManyField("Statement", blank=True)

    def soup(self):
        return TexSoup(self.manual_tex)
    def try_soup(self):
        try:
            return self.soup()
        except:
            return None
    def refs(self):
        return [ref.args[0] for ref in self.soup().find_all('ref')]
    def try_refs(self):
        try:
            return self.refs()
        except:
            return []

    def __str__(self):
        return "{} - {}".format(self.kind, self.name)
    class Meta:
        db_table = u'statements'

# Понятие
# Например, множество комплексных чисел
class Concept(BaseModel):
    name = models.CharField(max_length=255) # fallback названия
    # В разных курсах могут быть разные определения
    def __str__(self):
        return self.name
    class Meta:
        db_table = u'concepts'


class ConceptUsage(BaseModel):
    concept = models.ForeignKey(Concept, on_delete=models.CASCADE)
    statement = models.ForeignKey(Statement, on_delete=models.CASCADE)
    class Meta:
        db_table = u'concept_usages'
