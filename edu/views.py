from django.shortcuts import render
from edu.tasks import tex2json

from django.http import HttpResponse

# Create your views here.


def index(request):
    return HttpResponse("I am edu#index. Find me in {}".format(__file__))


def ast(request):
    if request.method == "POST":
        return HttpResponse(
            tex2json(request.body.decode("utf-8")), content_type="application/json"
        )
    else:
        return HttpResponse("not found")
