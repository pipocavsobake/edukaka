# Create your tasks here
from __future__ import absolute_import, unicode_literals
from celery import shared_task
from pathlib import Path

# import pickle
import marshal
from TexSoup import TexSoup
from .models import Course
import re

from json import JSONEncoder
import json

from django.core.files import File


class TeXEncoder(JSONEncoder):
    def default(self, o):
        d = o.__dict__
        return dict((k, d[k]) for k in d if k != "parent")


def prepare_tex(source):
    # https://github.com/alvinwan/TexSoup/issues/55
    ret = re.sub("\\\\\$", "\\\\ $", source)
    # with open("tmp.tex", "w") as f:
    # f.write(ret)
    return ret


@shared_task
def parse_tex_file(id):
    course = Course.objects.get(pk=id)
    ext = Path(course.source.path).suffixes[-1]
    if ext != ".tex" and ext != ".sty":
        raise ValueError("«" + course.source.path + "» doesn't have «.tex» extension")
    fl = open(course.source.path, "r")  # хочу raise, если не открылся
    tree = TexSoup(prepare_tex(fl.read()))
    tmp_path = f"tmp/tree_{course.id}.dat"
    json.dump(tree, open(tmp_path, "w"), cls=TeXEncoder, ensure_ascii=False)
    # import code; code.interact(local=dict(globals(), **locals()))
    course.json_file.save(f"tree_{course.id}.json", File(open(tmp_path, "r")))
    return tmp_path


def tex2json(tex):
    tree = TexSoup(prepare_tex(tex))
    return json.dumps(tree, cls=TeXEncoder, ensure_ascii=False)
