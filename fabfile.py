import random
from fabric.contrib.files import append, exists
from fabric.api import cd, env, local, run, prefix
import datetime
import os

env.user = "edu"
env.hosts = "shirykalov.ru"

def uname():
  run('uname -a')
  run('whoami')

REPO_URL = 'git@gitlab.com:pipocavsobake/edukaka.git'
LINKED_DIRS = ["public/media", "__pycache__", "tmp", "public/static"]
LINKED_FILES = ["config/database.yml"]
MAX_RELEASES = 10
GUNICORN_PID_FILE = "tmp/pids/gunicorn"
CELERY_PID_FILE = "tmp/pids/celery"

def stop():
    current_dir = f'/home/{env.user}/app/current'
    run(f'kill `cat {current_dir}/{GUNICORN_PID_FILE}`')
def start():
    current_dir = f'/home/{env.user}/app/current'
    lenv = 'lenv'
    with cd(current_dir), prefix(f'source {lenv}/bin/activate'):
        run("gunicorn -c gunicorn.py.ini -D edukaka.wsgi")


def deploy():
    app_dir = f'/home/{env.user}/app'
    shared_dir = f'{app_dir}/shared'
    releases_dir = f'{app_dir}/releases'
    repo_path = f'{app_dir}/repo'
    revision = os.environ.get('REVISION', "master")
    lenv = 'lenv'

    python, pip = _python_get_executables()

    t = str(run("date +%Y%m%d%H%M%S"))

    release_path = f'{releases_dir}/{t}'

    _git_check()
    _deploy_check_directories([shared_dir, releases_dir])
    with cd(shared_dir):
        _deploy_check_linked_dirs(LINKED_DIRS)
        _deploy_check_make_linked_dirs(LINKED_FILES)
    with cd(app_dir):
        _git_clone(repo_path)
    with cd(repo_path):
        _git_update()
        _git_create_release(revision, release_path)
        current_revision = _git_fetch_revision(revision)
    with cd(release_path):
        _deploy_set_current_revision(current_revision)
        _deploy_symlink_linked_files(shared_dir, release_path, LINKED_FILES + LINKED_DIRS)
        _python_virtual_env(python, pip, lenv)
    with cd(release_path), prefix(f'source {lenv}/bin/activate'):
        _django_db_migrate()
        _django_collect_static()
        _assets()
    _deploy_simlink_release(release_path, releases_dir, app_dir)
    with cd(releases_dir):
        _deploy_cleanup()
    with cd(app_dir):
        _deploy_log_revision(revision, current_revision, t)
    with cd(release_path), prefix(f'source {lenv}/bin/activate'):
        _gunicorn_restart()
        _celery_restart()



def _git_check():
    run(f'git ls-remote {REPO_URL} HEAD')
def _deploy_check_directories(dirs):
    joined = " ".join(dirs)
    run(f'mkdir -p {joined}')
def _deploy_check_linked_dirs(dirs):
    joined = " ".join(dirs)
    run(f'mkdir -p {joined}')
def _deploy_check_make_linked_dirs(files):
    dirs = [parent for parent in [os.path.dirname(linked_file) for linked_file in files] if len(parent) != 0]
    joined = " ".join(dirs)
    run(f'mkdir -p {joined}')
def _git_clone(repo_path="repo"):
    if not exists(f'{repo_path}/HEAD'):
        run(f'git clone --mirror {REPO_URL} {repo_path}')
def _git_update():
    run(f'git remote set-url origin {REPO_URL}')
    run(f'git remote update --prune')
def _git_create_release(revision, release_path):
    run(f'mkdir -p {release_path}')
    run(f'git archive {revision} | /usr/bin/env tar -x -f - -C {release_path}')
def _git_fetch_revision(revision):
    return str(run(f'git rev-list --max-count=1 {revision}'))
def _deploy_set_current_revision(revision):
    run(f'echo {revision} > REVISION')
def _deploy_symlink_linked_files(shared_path, release_path, files):
    for linked_file in files:
        if exists(linked_file):
            run(f'rm -rf {linked_file}')
        run(f'ln -s {shared_path}/{linked_file} {release_path}/{linked_file}')

def _python_get_executables():
    if str(run(f'python -V')).split()[1][0] == '2':
        return ("python3", "pip3")
    else:
        return ("python", "pip")
def _python_virtual_env(python, pip, lenv):
    run(f'{python} -m venv {lenv}')
    with prefix(f"source {lenv}/bin/activate"):
        run(f'which python')
        run(f'python -V')
        run(f'pip install --upgrade --quiet pip')
        run(f'pip -V')
        run(f'pip install --quiet -r requirements.txt')

def _django_db_migrate():
    run("python manage.py migrate")
def _django_collect_static():
    run("python manage.py collectstatic --noinput")

def _deploy_simlink_release(release_path, releases_path, app_path):
    run(f'ln -s {release_path} {releases_path}/current')
    run(f'mv {releases_path}/current {app_path}')
def _deploy_cleanup():
    releases = str(run('ls')).split()
    releases.sort(reverse=True)
    for release in releases[MAX_RELEASES:]:
        run(f'rm -rf {release}')
def _deploy_log_revision(revision, current_revision, t):
    user = str(local('whoami'))
    append("revisions.log", f'Branch {revision} (at {current_revision}) deployed as release {t} by {user}')
def _gunicorn_restart():
    if exists(GUNICORN_PID_FILE):
        run(f"kill `cat {GUNICORN_PID_FILE}`")
    run("gunicorn -c gunicorn.py.ini -D edukaka.wsgi")
def _celery_restart():
    if exists(CELERY_PID_FILE):
        run(f"kill `cat {CELERY_PID_FILE}`")
    run(f"celery --pidfile={CELERY_PID_FILE} -D -A edukaka worker")


# TODO fix reload without downtime
def _gunicorn_restart_not_works():
    if exists(GUNICORN_PID_FILE):
        run(f"kill -HUP `cat {GUNICORN_PID_FILE}`")
    else:
        run("gunicorn -c gunicorn.py.ini -D edukaka.wsgi")

def _assets():
    run(f"yarn install")
    run(f"yarn build")

