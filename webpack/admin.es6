import { debounce } from "lodash";
import katex from "katex";
import axios from "axios";

axios.defaults.headers.common["X-CSRFToken"] = document.querySelector(
  "meta[name=csrf-token]"
).content;
axios.defaults.headers.common["X-Requested-With"] = "XMLHttpRequest";

window.katex = katex;
const startTypeSetting = function() {
  if (django.jQuery("#statement-preview").length === 0)
    django
      .jQuery(".field-manual_tex")
      .append("<div id='statement-preview'></div>");
  const value = django.jQuery(this).val();
  django.jQuery("#statement-preview").html(value);
  MathJax.Hub.Queue(["Typeset", MathJax.Hub, "statement-preview"]);
  axios
    .post("/edu/ast", value)
    .then(resp => {
      console.log(resp.data);
    })
    .catch(err => {
      console.error(err);
    });
};

function init() {
  if (window.MathJax) {
    MathJax.Hub.Config({
      skipStartupTypeset: true,
      extensions: ["tex2jax.js", "TeX/AMSmath.js"],
      jax: ["input/TeX", "output/HTML-CSS"],
      tex2jax: {
        inlineMath: [["$", "$"], ["\\(", "\\)"]]
      }
    });
    django
      .jQuery("#id_manual_tex")
      .on("input", debounce(startTypeSetting, 500));
    console.log("initialized");
  } else {
    setTimeout(init, 100);
  }
}

django.jQuery(init);
