import React from "react";
import { render } from "react-dom";
import "./application.sass";
import App from "./App";

const init = () => {
  render(<App />, document.getElementById("root"));
};

document.addEventListener("DOMContentLoaded", init);
