import React from "react";
import ReactDOM from "react-dom";
import DataProvider from "~/components/DataProvider";
import Table from "~/components/Table";
const App = () => (
  <DataProvider
    endpoint="/api/v1/articles/"
    render={data => <Table data={data} />}
  />
);

export default App;
